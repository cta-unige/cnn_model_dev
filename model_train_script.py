import os
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from pathlib import Path
from livelossplot.inputs.keras import PlotLossesCallback
import tensorflow as tf

import json
import getopt
import sys

from classes.model import model_generator

param_line = '-i <input_path> -m <model> -o <output_path> -f <filter_size>'

'''
 input parametets:
 -i - input path to train/test and validation datesets
 -o - puth to output folder
 -f - model filter size (if applicable)
 -n - number of filters to be used (if applicable)
 -e - number of epochs
 -w - model wheights to be loaded before train (if applicable)
 -m - model to be used, like: vgg16, resnet_50, resnet_1stg, resnet_2stg
 -d - dropout
'''


try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:f:n:e:w:m:d:", ["input_path", "output_path", "filter_size", "n_filters", "epochs", "weight", "model", "dropout"])
except getopt.GetoptError:
    print('error: ',param_line)
    sys.exit(2)

dropout_to_use = 0

for opt, arg in opts:
    if opt == '-h':
        print(param_line)
        sys.exit()

    elif opt in ("-i", "--input_path"):
        input_path = arg

    elif opt in ("-o", "--output_path"):
        output_path = arg

    elif opt in ("-n", "--n_filters"):
        number_of_filters = int(arg)

    elif opt in ("-f", "--filter_size"):
        filter_size = int(arg)

    elif opt in ("-e", "--epochs"):
        n_epochs = int(arg)

    elif opt in ("-w", "--weight"):
        weights = arg
    
    elif opt in ("-m", "--model"):
        model_to_use = arg
    elif opt in ("-d", "--dropout"):
        dropout_to_use = float(arg)

defualt_weights = '/home/nagaia/ctasoft/scripts/cnn_model_dev/vgg16_default_model_weights/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'

print('input  path : ', input_path)
print('output files : ', output_path)

try:
    print('number of filter : ', number_of_filters)
    print('filter size : ', filter_size)
    print('vgg16 weights default : ', defualt_weights)
except:
    print('something not defined')

my_model = model_generator()

input_shape = (234, 234, 3)
optim_1 = Adam(learning_rate=0.001)
n_classes=4
n_layer_tune=0
#BATCH_SIZE = 64
BATCH_SIZE = 64

# First we'll train the model without Fine-tuning

if model_to_use == "vgg16":
    print('use vgg16 model')
    try:
        model_1l = my_model.create_vgg16_model(weights, input_shape, n_classes, optim_1, n_layer_tune)
        print('vgg16 :: load veights')
    except:
        model_1l = my_model.create_vgg16_model(defualt_weights, input_shape, n_classes, optim_1, n_layer_tune)
        print('vgg16 :: use def weights')

elif model_to_use == "resnet_2stg":
    print('use ResNet model with 2 stages')
    model_1l = my_model.resnet_2stages(input_shape, n_classes, optim_1, dropout_to_use)

elif model_to_use == "resnet_1stg":
    print('use ResNet model with 1 stages')
    model_1l = my_model.resnet_1stages(input_shape, n_classes, optim_1)

elif model_to_use == "resnet_50":
    print('use ResNet 50 model')
    model_1l = my_model.resnet50(input_shape, n_classes, optim_1, dropout_to_use)

else:
    print("Error ::  Unknown Model :: ", model_to_use)
    sys.exit()

#model_1l = my_model.create_vgg16_model(defualt_weights, input_shape, n_classes, optim_1, n_layer_tune)

try:
    #if (model_to_use != "vgg16"):
    model_1l.load_weights(weights)
    print('loaded weights : ', weights)
except:
    print('no weights were specified')

print(model_1l.summary())

physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs:", len(physical_devices))


train_generator = ImageDataGenerator(rotation_range=270,
                                     brightness_range=None,
                                     width_shift_range=0.05,
                                     height_shift_range=0.05,
                                     horizontal_flip=True,
                                     vertical_flip=True,
                                     validation_split=0.0,
                                     preprocessing_function=preprocess_input) # VGG16 preprocessing

#train_generator = ImageDataGenerator(rotation_range=90,
#                                     brightness_range=None,
#                                     width_shift_range=0.0,
#                                     height_shift_range=0.0,
#                                     horizontal_flip=False,
#                                     vertical_flip=False,
#                                     validation_split=0.0,
#                                     preprocessing_function=preprocess_input) # VGG16 preprocessing

test_generator = ImageDataGenerator(preprocessing_function=preprocess_input) # VGG16 preprocessing

dir_path = Path(input_path)


train_data_dir = dir_path/'train'
test_data_dir = dir_path/'test'
validation_data_dir = dir_path/'validation'

#class_subset = sorted(os.listdir(download_dir/'food-101/images'))[:10] # Using only the first 10 classes

traingen = train_generator.flow_from_directory(train_data_dir,
                                               target_size=(234, 234),
                                               class_mode='categorical',
                                               batch_size=BATCH_SIZE,
                                               shuffle=True,
                                               seed=42)
test_generator = ImageDataGenerator(preprocessing_function=preprocess_input)

print('train, n files : ', len(traingen.classes), ' classes : ', traingen.class_indices)

validgen = train_generator.flow_from_directory(validation_data_dir,
                                               target_size=(234, 234),
                                               class_mode='categorical',
                                               batch_size=BATCH_SIZE,
                                               shuffle=True,
                                               seed=42)
print('validation, n files : ', len(validgen.classes), ' classes : ', validgen.class_indices)

testgen = test_generator.flow_from_directory(test_data_dir,
                                             target_size=(234, 234),
                                             class_mode=None,
                                             batch_size=1,
                                             shuffle=False,
                                             seed=42)
print('test, n files : ', len(testgen.classes), ' classes : ', testgen.class_indices)

n_steps = traingen.samples // BATCH_SIZE
n_val_steps = validgen.samples // BATCH_SIZE
#n_epochs = 70

plot_loss_1 = PlotLossesCallback()

# ModelCheckpoint callback - save best weights
tl_checkpoint_1 = ModelCheckpoint(filepath=output_path + '.weights.best.hdf5',
                                  save_best_only=True,
                                  verbose=1)

# EarlyStopping
early_stop = EarlyStopping(monitor='val_loss',
                           patience=10,
                           restore_best_weights=True,
                           mode='min')

vgg_history = model_1l.fit(traingen,
                            batch_size=BATCH_SIZE,
                            epochs=n_epochs,
                            validation_data=validgen,
                            steps_per_epoch=n_steps,
                            validation_steps=n_val_steps,
                            callbacks=[tl_checkpoint_1, early_stop, plot_loss_1],
                            verbose=1)

with open(output_path + '.json', 'w') as f:
    json.dump(vgg_history.history, f)
