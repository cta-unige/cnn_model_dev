import os, sys
import json
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os.path
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import numpy as np
import getopt
import tensorflow as tf
from numpy import asarray
from numpy import save

from classes.prepare_data import data_split
from classes.model_validation import model_validation

param_line = '-i <input_path> -m <model> -o <output_path> -w <weight>'
drop_out = 0.

try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:w:m:c:", ["input_path", "output_path", "weight", "model", "classes"])
except getopt.GetoptError:
    print('error: ',param_line)
    sys.exit(2)

for opt, arg in opts:
    if opt == '-h':
        print(param_line)
        sys.exit()

    elif opt in ("-i", "--input_path"):
        input_path = arg

    elif opt in ("-o", "--output_path"):
        output_path = arg

    elif opt in ("-w", "--weight"):
        weights = arg
    
    elif opt in ("-m", "--model"):
        model_to_use = arg
    
    elif opt in ("-c", "--classes"):
        n_classes = int(arg)

my_model = model_validation()

print('use : ', n_classes, ' classes')

input_shape = (234, 234, 3)
optim_1 = Adam(learning_rate=0.001)
#n_classes=4
n_layer_tune=0

defualt_weights = '/home/nagaia/ctasoft/scripts/cnn_model_dev/vgg16_default_model_weights/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'


if model_to_use == "vgg16":
    print('use vgg16 model')
    try:
        model_1l = my_model.create_vgg16_model(weights, input_shape, n_classes, optim_1, n_layer_tune)
        print('vgg16 :: load veights')
    except:
        model_1l = my_model.create_vgg16_model(defualt_weights, input_shape, n_classes, optim_1, n_layer_tune)
        print('vgg16 :: use def weights')

elif model_to_use == "resnet_2stg":
    print('use ResNet model with 2 stages')
    model_1l = my_model.resnet_2stages(input_shape, n_classes, optim_1, drop_out)

elif model_to_use == "resnet_1stg":
    print('use ResNet model with 1 stages')
    model_1l = my_model.resnet_1stages(input_shape, n_classes, optim_1)

elif model_to_use == "resnet_50":
    print('use ResNet 50 model')
    model_1l = my_model.resnet50(input_shape, n_classes, optim_1, drop_out)

else:
    print("Error ::  Unknown Model :: ", model_to_use)
    sys.exit()

try:
    #if (model_to_use != "vgg16"):
    model_1l.load_weights(weights)
    print('loaded weights : ', weights)
except:
    print('no weights were specified')

print(model_1l.summary())

print('-----------------------')

physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs:", len(physical_devices))
print('-----------------------')



test_generator = ImageDataGenerator(preprocessing_function=preprocess_input)

testgen = test_generator.flow_from_directory(input_path,
                                             target_size=(234, 234),
                                             class_mode=None,
                                             batch_size=1,
                                             shuffle=False,
                                             seed=42)


true_classes = testgen.classes
class_indices = testgen.class_indices
class_indices = dict((v,k) for k,v in class_indices.items())

model_preds = model_1l.predict(testgen)
model_pred_classes = np.argmax(model_preds, axis=1)

model_acc = accuracy_score(true_classes, model_pred_classes)
print("Model Accuracy : {:.2f}%".format(model_acc * 100))

class_names = testgen.class_indices.keys()

fig, ax3 = plt.subplots(1, 1, figsize=(10, 10))
my_model.plot_heatmap(true_classes, model_pred_classes, class_names, ax3, title="")
fig.tight_layout()
plt.savefig(os.path.join(output_path + '/heat_map_' + str(model_to_use) + 'results_200k_nsb.png'), dpi=200)

shower_reco_wrong = []
nsb_reco_wrong = []
proton_reco_wrong = []
muon_reco_wrong = []

showers_correct_reco = []
nsb_correct_reco = []
muon_correct_reco = []
proton_correct_reco = []

save(output_path + '/' + str(model_to_use) + '_files_200k_nsb.npy', testgen.filenames)
save(output_path + '/' + str(model_to_use) + '_true_classes_data_200k_nsb.npy', true_classes)
save(output_path + '/' + str(model_to_use) + '_model_pred_classes_data_200k_nsb.npy', model_preds)

'''
0 = muon
1 = nsb
2 = proton
3 = shower
'''

'''

for item, (t_class, p_class) in enumerate(zip(true_classes, model_pred_classes)):
    if t_class != p_class:
        #print(item, ' ', t_class, ' ', p_class, ' ', testgen.filenames[item])
        if t_class == 0:
            muon_reco_wrong.append(input_path + testgen.filenames[item])
        elif t_class == 1:
            nsb_reco_wrong.append(input_path + testgen.filenames[item])
        elif t_class == 2:
            proton_reco_wrong.append(input_path + testgen.filenames[item])
        elif t_class == 3:
            shower_reco_wrong.append(input_path + testgen.filenames[item])
    else:
        if t_class == 0:
            muon_correct_reco.append(input_path + testgen.filenames[item])
        elif t_class == 1:
            nsb_correct_reco.append(input_path + testgen.filenames[item])
        elif t_class == 1:
            proton_correct_reco.append(input_path + testgen.filenames[item])
        elif t_class == 2:
            showers_correct_reco.append(input_path + testgen.filenames[item])


with open(output_path + '/data_types/' + str(model_to_use)  +'_data_test_011_muon_correct_files.json', 'w') as f:
    json.dump(muon_correct_reco, f)
with open(output_path + '/data_types/' + str(model_to_use)  +'data_test_011_nsb_correct_files.json', 'w') as f:
    json.dump(nsb_correct_reco, f)
with open(output_path + '/data_types/' + str(model_to_use) +'_data_test_011_proton_correct_files.json', 'w') as f:
    json.dump(proton_correct_reco, f)
with open(output_path + '/data_types/' + str(model_to_use) +'_data_test_011_showrs_correct_files.json', 'w') as f:
    json.dump(showers_correct_reco, f)

with open(output_path + '/data_types/' + str(model_to_use) +'_data_test_011_muon_wrong_files.json', 'w') as f:
    json.dump(muon_reco_wrong, f)
with open(output_path + '/data_types/' + str(model_to_use) +'_data_test_011_nsb_wrong_files.json', 'w') as f:
    json.dump(nsb_reco_wrong, f)
with open(output_path + '/data_types/' + str(model_to_use) +'_data_test_011_proton_wrong_files.json', 'w') as f:
    json.dump(proton_reco_wrong, f)
with open(output_path + '/data_types/' + str(model_to_use) +'_data_test_011_showrs_wrong_files.json', 'w') as f:
    json.dump(shower_reco_wrong, f)


sowers = data_split()

print("showers_correct_reco : ", showers_correct_reco)

data_shower_correct = sowers.get_data_statistics_single_file(showers_correct_reco, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_showrs_correct.json', 'w') as f:
    json.dump(data_shower_correct, f)

data_shower_wrong = sowers.get_data_statistics_single_file(shower_reco_wrong, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_showrs_wrong.json', 'w') as f:
    json.dump(data_shower_wrong, f)

data_nsb_correct = sowers.get_data_statistics_single_file(nsb_correct_reco, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_nsb_correct.json', 'w') as f:
    json.dump(data_nsb_correct, f)

data_nsb_wrong = sowers.get_data_statistics_single_file(nsb_reco_wrong, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_nsb_wrong.json', 'w') as f:
    json.dump(data_nsb_wrong, f)

data_proton_correct = sowers.get_data_statistics_single_file(proton_correct_reco, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_proton_correct.json', 'w') as f:
    json.dump(data_proton_correct, f)

data_proton_wrong = sowers.get_data_statistics_single_file(proton_reco_wrong, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_proton_wrong.json', 'w') as f:
    json.dump(data_proton_wrong, f)

data_muon_correct = sowers.get_data_statistics_single_file(muon_correct_reco, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_muon_correct.json', 'w') as f:
    json.dump(data_muon_correct, f)

data_muon_wrong = sowers.get_data_statistics_single_file(muon_reco_wrong, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open(output_path + '/data_types/'+ str(model_to_use) +'_data_test_011_muon_wrong.json', 'w') as f:
    json.dump(data_muon_wrong, f)
'''

