import os
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.layers import Dense, Dropout, Flatten, Input, ZeroPadding2D, Conv2D, BatchNormalization, Activation, MaxPooling2D, Add, AveragePooling2D
from tensorflow.keras import Input
from tensorflow.keras import activations
from tensorflow.keras.regularizers import l2

class model_generator:

    def __init__(self):
        '''
        class to generate different CNN model used for trigger algorithm
        '''
        print('model generator start')

    def create_vgg16_model(self, input_model_weight, input_shape, n_classes, optimizer='rmsprop', fine_tune=0):

        '''
        Crate VGG16 based model
        :param input_model_weight: model weigth to be loaded
        :param input_shape:        input image shape, like (234, 234, 3)
        :param n_classes:          n data classes to be used, like: 2 = NSB and Shower, or 4 = NSB, gammas, electrons, protons
        :param optimizer:          Optimizer to use, i.e Adam
        :param fine_tune:          n layers (from the end) to be re-traind if transfer learning is used
        :return:
        '''

        conv_base = VGG16(include_top=False,
                          weights=input_model_weight,
                          input_shape=input_shape)

        if fine_tune > 0:
            for layer in conv_base.layers[:-fine_tune]:
                layer.trainable = False
        else:
            for layer in conv_base.layers:
                layer.trainable = False

        top_model = conv_base.output
        top_model = Flatten(name="flatten")(top_model)
        top_model = Dense(4096, activation='relu')(top_model)
        top_model = Dense(1072, activation='relu')(top_model)
        top_model = Dropout(0.2)(top_model)
        output_layer = Dense(n_classes, activation='softmax')(top_model)

        # Group the convolutional base and new fully-connected layers into a Model object.
        model = Model(inputs=conv_base.input, outputs=output_layer)

        # Compiles the model for training.
        model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

        return model

    def create_vgg16_1_layer(self, input_shape, n_classes, optimizer='rmsprop', n_filters = 32, filter_size = 3):

        '''

        :param input_shape: input image shape, like = (234, 234, 3)
        :param n_classes: number of classse, like "shower" & "nsb"
        :param optimizer: model optimizer
        :param filter_size: filer size, def = 3
        :return: cnn model
        '''

        print(' n filters : ', n_filters, ' filter size : ', filter_size)
        model = Sequential()
        model.add(Input(shape=input_shape))
        model.add(Conv2D(n_filters, (filter_size, filter_size), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(MaxPooling2D((7, 7)))
        model.add(Flatten())
        model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
        model.add(Dense(n_classes, activation='softmax'))
        # compile model

        model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
        return model



    def create_vgg16_1_block(self, input_shape, n_classes, optimizer='rmsprop', n_filters = 32, filter_size=3):

        '''

        :param input_shape: input image shape, like = (234, 234, 3)
        :param n_classes: number of classse, like "shower" & "nsb"
        :param optimizer: model optimizer
        :param filter_size: filer size, def = 3
        :return: cnn model
        '''

        print('n filters : {} of {} x {}'.format(n_filters, filter_size, filter_size))

        model = Sequential()
        model.add(Input(shape=input_shape))
        model.add(Conv2D(n_filters, (filter_size, filter_size), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(MaxPooling2D((2, 2)))
        model.add(Conv2D(n_filters, (filter_size, filter_size), activation='relu', kernel_initializer='he_uniform', padding='same'))
        model.add(MaxPooling2D((2, 2)))

        model.add(Flatten())
        model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
        model.add(Dense(n_classes, activation='softmax'))
        # compile model

        model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
        return model

    def res_identity(self, x, filters):
        # renet block where dimension doesnot change.
        # The skip connection is just simple identity conncection
        # we will have 3 blocks and then input will be added

        x_skip = x  # this will be used for addition with the residual block
        f1, f2 = filters

        # first block
        x = Conv2D(f1, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=l2(0.001))(x)
        x = BatchNormalization()(x)
        x = Activation(activations.relu)(x)

        # second block # bottleneck (but size kept same with padding)
        x = Conv2D(f1, kernel_size=(3, 3), strides=(1, 1), padding='same', kernel_regularizer=l2(0.001))(x)
        x = BatchNormalization()(x)
        x = Activation(activations.relu)(x)

        # third block activation used after adding the input
        x = Conv2D(f2, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=l2(0.001))(x)
        x = BatchNormalization()(x)
        # x = Activation(activations.relu)(x)

        # add the input
        x = Add()([x, x_skip])
        x = Activation(activations.relu)(x)

        return x

    def res_conv(self, x, s, filters):
        '''
        here the input size changes'''
        x_skip = x
        f1, f2 = filters

        # first block
        x = Conv2D(f1, kernel_size=(1, 1), strides=(s, s), padding='valid', kernel_regularizer=l2(0.001))(x)
        # when s = 2 then it is like downsizing the feature map
        x = BatchNormalization()(x)
        x = Activation(activations.relu)(x)

        # second block
        x = Conv2D(f1, kernel_size=(3, 3), strides=(1, 1), padding='same', kernel_regularizer=l2(0.001))(x)
        x = BatchNormalization()(x)
        x = Activation(activations.relu)(x)

        # third block
        x = Conv2D(f2, kernel_size=(1, 1), strides=(1, 1), padding='valid', kernel_regularizer=l2(0.001))(x)
        x = BatchNormalization()(x)

        # shortcut
        x_skip = Conv2D(f2, kernel_size=(1, 1), strides=(s, s), padding='valid', kernel_regularizer=l2(0.001))(x_skip)
        x_skip = BatchNormalization()(x_skip)

        # add
        x = Add()([x, x_skip])
        x = Activation(activations.relu)(x)

        return x

    def resnet50(self, input_shape, n_classes, optimizer, my_dropout):

        '''
        Crate ResNet 50 based model
        :param input_shape:        input image shape, like = (234, 234, 3)
        :param n_classes:          n data classes to be used, like: 2 = NSB and Shower, or 4 = NSB, gammas, electrons, protons
        :param optimizer:          Optimizer to use, i.e Adam
        :param my_dropout:         dropout
        :return:
        '''



        input_im = Input(shape=input_shape)  # cifar 10 images size
        x = ZeroPadding2D(padding=(3, 3))(input_im)

        # 1st stage
        # here we perform maxpooling, see the figure above

        x = Conv2D(64, kernel_size=(7, 7), strides=(2, 2))(x)
        x = BatchNormalization()(x)
        x = Activation(activations.relu)(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)

        # 2nd stage
        # frm here on only conv block and identity block, no pooling

        x = self.res_conv(x, s=1, filters=(64, 256))
        x = self.res_identity(x, filters=(64, 256))
        x = self.res_identity(x, filters=(64, 256))

        # 3rd stage

        x = self.res_conv(x, s=2, filters=(128, 512))
        x = self.res_identity(x, filters=(128, 512))
        x = self.res_identity(x, filters=(128, 512))
        x = self.res_identity(x, filters=(128, 512))

        # 4th stage

        x = self.res_conv(x, s=2, filters=(256, 1024))
        x = self.res_identity(x, filters=(256, 1024))
        x = self.res_identity(x, filters=(256, 1024))
        x = self.res_identity(x, filters=(256, 1024))
        x = self.res_identity(x, filters=(256, 1024))
        x = self.res_identity(x, filters=(256, 1024))

        # 5th stage

        x = self.res_conv(x, s=2, filters=(512, 2048))
        x = self.res_identity(x, filters=(512, 2048))
        x = self.res_identity(x, filters=(512, 2048))

        # ends with average pooling and dense connection

        x = AveragePooling2D((2, 2), padding='same')(x)

        x = Flatten()(x)
        if my_dropout > 0:
            print('use Dropout of : ', my_dropout)
            x = Dropout(my_dropout)(x)
        x = Dense(n_classes, activation='softmax', kernel_initializer='he_normal')(x)  # multi-class

        # define the model

        model = Model(inputs=input_im, outputs=x, name='Resnet50')

        model.compile(optimizer=optimizer,
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    def resnet_2stages(self, input_shape, n_classes, optimizer, my_dropout):

        '''
          Crate ResNet 50 model just with 2 first stages
          :param input_shape:        input image shape, like = (234, 234, 3)
          :param n_classes:          n data classes to be used, like: 2 = NSB and Shower, or 4 = NSB, gammas, electrons, protons
          :param optimizer:          Optimizer to use, i.e Adam
          :param my_dropout:         dropout
          :return:
        '''

        input_im = Input(shape=input_shape)  # cifar 10 images size
        x = ZeroPadding2D(padding=(3, 3))(input_im)

        # 1st stage
        # here we perform maxpooling, see the figure above

        x = Conv2D(64, kernel_size=(7, 7), strides=(2, 2))(x)
        x = BatchNormalization()(x)
        x = Activation(activations.relu)(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)

        # 2nd stage
        # frm here on only conv block and identity block, no pooling

        x = self.res_conv(x, s=1, filters=(64, 256))
        x = self.res_identity(x, filters=(64, 256))
        x = self.res_identity(x, filters=(64, 256))

        # ends with average pooling and dense connection

        x = AveragePooling2D((2, 2), padding='same')(x)

        x = Flatten()(x)

        if my_dropout > 0:
            print('use Dropout of : ', my_dropout)
            x = Dropout(my_dropout)(x)

        x = Dense(n_classes, activation='softmax', kernel_initializer='he_normal')(x)  # multi-class

        # define the model

        model = Model(inputs=input_im, outputs=x, name='ResNet_2_block')

        model.compile(optimizer=optimizer,
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        return model

    def resnet_1stages(self, input_shape, n_classes, optimizer):

        '''
        Crate ResNet 50 model just with 1 first stages
        :param input_shape:        input image shape, like = (234, 234, 3)
        :param n_classes:          n data classes to be used, like: 2 = NSB and Shower, or 4 = NSB, gammas, electrons, protons
        :param optimizer:          Optimizer to use, i.e Adam
        :param my_dropout:         dropout
        :return:
        '''

        input_im = Input(shape=input_shape)  # cifar 10 images size
        x = ZeroPadding2D(padding=(3, 3))(input_im)

        # 1st stage
        # here we perform maxpooling, see the figure above

        x = Conv2D(64, kernel_size=(7, 7), strides=(2, 2))(x)
        x = BatchNormalization()(x)
        x = Activation(activations.relu)(x)
        x = MaxPooling2D((3, 3), strides=(2, 2))(x)

        # ends with average pooling and dense connection

        x = AveragePooling2D((2, 2), padding='same')(x)

        x = Flatten()(x)
        x = Dense(n_classes, activation='softmax', kernel_initializer='he_normal')(x)  # multi-class

        # define the model

        model = Model(inputs=input_im, outputs=x, name='ResNet_1_block')

        model.compile(optimizer=optimizer,
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])

        return model

