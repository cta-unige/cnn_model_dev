import os
import glob
from os import listdir
from os.path import join, isfile
import shutil
import random
import pandas as pd
import numpy as np

class data_split:

    def __init__(self):
        '''
        generate create data split of train test and validation
        '''
        print('data split start')

    def copy_some_files(self, input_data_path, out_path, n_files):

        '''
        Copy some random number of files from one folder to another
        :param input_data_path: path to original folder with data
        :param out_path: path to output folder
        :param n_files: number of random file to be copied
        '''

        files_input = listdir(input_data_path)
        numbers = list(range(0, len(files_input)))
        random.shuffle(numbers)
        numbers = numbers[:n_files]

        for i_image in numbers:
            try:
                shutil.copy(input_data_path + files_input[i_image], out_path)
            except shutil.Error as e:
                print('Error: %s' % e)
            except IOError as e:
                print('Error: %s' % e.strerror)

    def copy_all_types(self, input_data_path, out_path, image_type, anomaly, n_files):

        '''
        Copy all files with a given anomaly score and shower type from one folder to another

        :param input_data_path: path to original folder with data
        :param out_path: path to output folder
        :param image_type: type of images, like clipped, normalized or original
        :param anomaly: anomaly score to use, typicaly 0.11 or 0.2 for showers and 0.001 for nsb
        :param n_files: number of random file to be copied
        :return:
        '''

        anomaly_path = 'anomaly_' + str(anomaly)
        self.showers_type = ['gamma_diffuse_nsbx1', 'gamma_on_nsbx1', 'electron_nsbx1', 'proton_nsbx1']

        for i_shower in self.showers_type:
            input_path = input_data_path + i_shower + '/images/'+ anomaly_path + image_type
            print("input : ", input_path)
            print("output: ", out_path)

            self.copy_some_files(input_path, out_path, n_files)

    def copy_some_showers(self, input_data_path, out_path, image_type, showers_type, anomaly, n_files):

        '''
        Copy some random number of files with a given anomaly score and shower type from one folder to another

        :param input_data_path: path to original folder with data
        :param out_path: path to output folder
        :param image_type: type of images, like clipped, normalized or original
        :param showers_type: type of showers to be copied, like: electron, proton, gamma_on or gamma_diffuse
        :param anomaly: anomaly score to use, typicaly 0.11 or 0.2 for showers and 0.001 for nsb
        :param n_files: number of random file to be copied
        :return:
        '''

        anomaly_path = 'anomaly_' + str(anomaly)
        self.showers_type = showers_type

        for i_shower in self.showers_type:
            input_path = input_data_path + i_shower + '/images/'+ anomaly_path + image_type
            print("input : ", input_path)
            print("output: ", out_path)

            if n_files == 'all':
                self.copy_all_files_without_split(input_path, out_path)
            else:
                self.copy_some_files_without_split(input_path, out_path, n_files)


    def copy_all_types_all_files(self, input_data_path, out_path, image_type, anomaly, data_class):

        '''
        Copy all files of all types ('gamma_diffuse_nsbx1', 'gamma_on_nsbx1', 'electron_nsbx1', 'proton_nsbx1') with a given anomaly score from one folder to another

        :param input_data_path: path to original folder with data
        :param out_path: path to output folder
        :param image_type: type of images, like clipped, normalized or original
        :param anomaly: anomaly score to use, typicaly 0.11 or 0.2 for showers and 0.001 for nsb
        :param data_class: data class, like showere and NSB for 2 classes model or shower, NSB, muon and electron for 4 classes model
        :return:
        '''

        anomaly_path = 'anomaly_' + str(anomaly)
        self.showers_type = ['gamma_diffuse_nsbx1', 'gamma_on_nsbx1', 'electron_nsbx1', 'proton_nsbx1']

        for i_shower in self.showers_type:
            input_path = input_data_path + i_shower + '/images/'+ anomaly_path + image_type
            print("input : ", input_path)
            print("output: ", out_path)

            self.copy_all_files(input_path, out_path, data_class)

    def copy_all_files(self, input_path, output_path, data_class):

        '''
         Copy all files from single folder into another folder and separate it into 3: test, train and validation
        :param input_path: path to original folder with data
        :param output_path: path to output folder, which will also have test, train and validation subfolders
        :param data_class: data class, like showere and NSB for 2 classes model or shower, NSB, muon and electron for 4 classes model
        :return:
        '''

        train_dir = output_path + 'train/' + data_class
        test_dir = output_path + 'test/' + data_class
        validation_dir = output_path + 'validation/' + data_class

        files_input = listdir(input_path)

        self.create_dir(train_dir)
        self.create_dir(test_dir)
        self.create_dir(validation_dir)

        for i_image in range(len(files_input)):
            if (i_image) % 3 == 0:
                shutil.copy(input_path + files_input[i_image], train_dir)
            if (i_image) % 3 == 1:
                shutil.copy(input_path + files_input[i_image], test_dir)
            if (i_image) % 3 == 2:
                shutil.copy(input_path + files_input[i_image], validation_dir)

    def copy_all_files_without_split(self, input_path, output_path):

        '''
         Copy all files from single folder into another folder without test, train and validation split
        :param input_path: path to original folder with data
        :param output_path: path to output folder
        :return:
        '''

        files_input = listdir(input_path)


        for i_image in range(len(files_input)):
            shutil.copy(input_path + files_input[i_image], output_path)

    def copy_some_files_without_split(self, input_path, output_path, n_files):

        '''
        Copy some random number of files from one folder to another
        :param input_path:  path to original folder with data
        :param output_path: path to output folder
        :param n_files:     number of random file to be copied
        :return:
        '''

        files_input = listdir(input_path)
        numbers = list(range(0, len(files_input)))
        random.shuffle(numbers)
        numbers = numbers[:n_files]

        for i_image in numbers:
            shutil.copy(input_path + files_input[i_image], output_path)

    def create_dir(self, path):

        '''
        Create a folder
        :param path: path to a folder to be created
        :return:
        '''

        try:
            os.makedirs(path, exist_ok=False)
            print("Directory ", path, " Created ")
        except FileExistsError:
            print("Directory ", path, " already exists")



    def test_files(self, path, key_word):

        '''
        See how many files with a give key word exist in a folder
        :param path:
        :param key_word:
        :return:
        '''

        n_found = 0
        files_input = listdir(path)
        print('found {} files'.format(len(files_input)))
        for i_file in files_input:
            if i_file.find(key_word) != -1:
                n_found = n_found + 1
                #
        print('Found ', n_found)


    def split_data(self, input_data_path, out_data, my_class):

        '''
        Copy images from input folder to output and split it into train, test and validation
        :param input_data_path: path to input images to be moved
        :param out_data: path where train, test and validation split should be
        :param my_class: class name, like "shower" or "nsb"
        :return:
        '''

        train_dir = out_data + '/train/' + my_class
        test_dir = out_data + '/test/' + my_class
        validation_dir = out_data + '/validation/' + my_class

        try:
            os.makedirs(train_dir, exist_ok=False)
            print("Directory ", train_dir, " Created ")
        except FileExistsError:
            print("Directory ", train_dir, " already exists")

        try:
            os.makedirs(validation_dir, exist_ok=False)
            print("Directory ", validation_dir, " Created ")
        except FileExistsError:
            print("Directory ", validation_dir, " already exists")

        try:
            os.makedirs(test_dir, exist_ok=False)
            print("Directory ", test_dir, " Created ")
        except FileExistsError:
            print("Directory ", test_dir, " already exists")

        onlyfiles_signal = [f for f in listdir(input_data_path) if isfile(join(input_data_path, f))]
        print('we have {} signals'.format(len(onlyfiles_signal)))

        for i_image in range(len(onlyfiles_signal)):
            if (i_image) % 3 == 0:
                try:
                    shutil.move(input_data_path + onlyfiles_signal[i_image], train_dir)
                except shutil.Error as e:
                    print('Error: %s' % e)
                except IOError as e:
                    print('Error: %s' % e.strerror)
            if (i_image) % 3 == 1:
                try:
                    shutil.move(input_data_path + onlyfiles_signal[i_image], test_dir)
                except shutil.Error as e:
                    print('Error: %s' % e)
                except IOError as e:
                    print('Error: %s' % e.strerror)
            if (i_image) % 3 == 2:
                try:
                    shutil.move(input_data_path + onlyfiles_signal[i_image], validation_dir)
                except shutil.Error as e:
                    print('Error: %s' % e)
                except IOError as e:
                    print('Error: %s' % e.strerror)

    def get_all_files(self, input_path):
        '''
        Return the list of files in a folder
        :param input_path: path to input images
        :return:  list of files
        '''

        return listdir(input_path)

    def return_type_all(self, file):

        '''
        Get particle name from image name
        :param file: image name
        :return: shower_type, string of shower type: muon, gamma_on, gamma_diffused, electron or proton
        '''

        if file.find('_run') != -1:
            end_run = file.find('_run')
            shower_type = file[:end_run]
        elif file.find('muon') != -1:
            shower_type = file
        else:
            shower_type = 'undef'

        return shower_type

    def return_data_name_all(self, file):

        '''
        Get shower ID from file name
        :param file: input shower file
        :return: dictinary with shower info. : 'shower_type', 'run' and 'id'
        '''

        found_data = {}
        head, tail = os.path.split(file)

        run_number = 0
        shower_id = 0
        len_id = ('_id_')

        if tail.find('_run') != -1:
            end_run = tail.find('_run')
            shower_type = tail[:end_run]

            if shower_type.find('_') != -1:
                shower_type = shower_type[shower_type.find('_')+1:]

            id_index = tail.find('_id_')
            run_number = int(tail[end_run + len('_run_'):id_index])
            shower_id = int(tail[id_index + len('_id_'):-len('.jpg')])

            if shower_type.find('_nsbx1') != -1:
                shower_type = shower_type[:shower_type.find('_nsbx1')]
            if shower_type.find('nsb_') != -1:
                shower_type = shower_type[len('nsb_'):]

            if tail.find('electron') != -1:
                shower_type = 'electron'
            elif tail.find('gamma_on') != -1:
                shower_type = 'gamma_on'
            elif tail.find('gamma_diffuse') != -1:
                shower_type = 'gamma_diffuse'
            elif tail.find('proton') != -1:
                shower_type = 'proton'

            found_data = {'shower_type' : shower_type, 'run': run_number, 'id' : shower_id}

        elif file.find('muon') != -1:

            energy_gev, off_axis, phi = self.return_conditions_all(tail)

            id_index = tail.find('_id_')
            shower_id = int(tail[id_index + len('_id_'):-len('.jpg')])

            shower_type = 'muon'
            found_data = {'shower_type': shower_type, 'energy TeV.': energy_gev/1000., 'off_axis': off_axis, 'phi' : phi, 'id' : shower_id}

        else:
            shower_type = 'undef'
            run_number = 0
            shower_id = 0
            found_data = {'shower_type': shower_type}

        return found_data

    def return_conditions_all(self, file):

        '''
        Return condition at which muon shower wsa generated
        :param file: muon shower image name
        :return: energy, off_axis and phi
        '''

        start_GeV = file.find('muon__')
        end_GeV = file.find('_GeV')
        end_off_axis = file.find('_off_axis_')
        end_off_phi = file.find('_phi')

        energy_gev = file[start_GeV + len('muon__'):end_GeV]
        off_axis = file[end_GeV + len('_GeV_'):end_off_axis]
        phi = file[end_off_axis + len('_off_axis_'):end_off_phi]

        try:
            energy_tmp =float(energy_gev)
        except:
            energy_tmp = np.NaN
        try:
            off_axis_tmp =float(off_axis)
        except:
            off_axis_tmp = np.NaN
        try:
            phi_tmp =float(phi)
        except:
            phi_tmp = np.NaN

        return energy_tmp, off_axis_tmp, phi_tmp

    def find_xlsx_file(self, path, run_id):

        '''
        Find xlxs file with a given run id
        :param path: folder with all xlxs files
        :param run_id: shower run ID
        :return:
        '''

        files_found = glob.glob(path + '/*run' + str(run_id) +'.*', recursive=True)
        if len(files_found) == 1:
            #print('files_found : ', files_found)
            return files_found[0]
        else:
            print(len(files_found))

    def get_shower_data(self, path_df, id_shower):

        '''
        Get shower info from xlsx file and shower ID
        :param path_df: path to xlsx file
        :param id_shower: shower ID
        :return: df for a given ID and File
        '''

        df_showers = pd.read_excel(path_df, index_col=0)
        #print('df_showers id : ',df_showers[df_showers['ID'] == id_shower])

        return df_showers[df_showers['ID'] == id_shower]

    def get_wrong_dict(self, somethng_as_nsb, input_df_path):

        '''
        Get info about showers, which were classified as NBS
        :param somethng_as_nsb:  file name of image classified as NSB
        :param input_df_path: path to folder with xlsx file with shower features, used to generate images
        :return: dictionary with shower parameters, like 'shower_type', 'energy TeV.', 'asum', 'off_axis', 'phi'
        '''

        somethng_as_nsb_dict = {}

        for i_file in somethng_as_nsb:
            somethng_as_nsb_dict_tmp = self.get_data_statistics_single_file(i_file, input_df_path)

            somethng_as_nsb_dict = self.update_dictionary(somethng_as_nsb_dict, 'shower_type', somethng_as_nsb_dict_tmp['shower_type'])
            somethng_as_nsb_dict = self.update_dictionary(somethng_as_nsb_dict, 'energy TeV.', somethng_as_nsb_dict_tmp['energy TeV.'])
            somethng_as_nsb_dict = self.update_dictionary(somethng_as_nsb_dict, 'asum', somethng_as_nsb_dict_tmp['asum'])
            somethng_as_nsb_dict = self.update_dictionary(somethng_as_nsb_dict, 'off_axis', somethng_as_nsb_dict_tmp['off_axis'])
            somethng_as_nsb_dict = self.update_dictionary(somethng_as_nsb_dict, 'phi', somethng_as_nsb_dict_tmp['phi'])

        return somethng_as_nsb_dict


    def get_wrong_reco(self, predict_class, true_class, files_list, true_value, wrong_value):

        '''
        Get list of file with wrongly predicted class:
        :param predict_class: predicted class
        :param true_class:  true class
        :param files_list:  list of files used for prediction
        :param true_value:  value of true class
        :param wrong_value: value
        :return:
        '''
    
        results_list = []
    
        for index in range(len(predict_class)):
            if(true_class[index] == true_value):
                if predict_class[index] == wrong_value:
                    results_list.append(files_list[index])
        return results_list

    def get_muon_exel_file(self, muon_dict, path):
    
        files_all = [f for f in listdir(path) if isfile(join(path, f))]
    
        for ifile in files_all:
            if ifile.find(str( int(1000*muon_dict['energy TeV.']) ) ) != -1:
                if ifile.find(str(muon_dict['off_axis'])) != -1:
                    if ifile.find(str(muon_dict['phi'])) != -1:
                        file_found = ifile
        return file_found


    def get_data_statistics_single_file(self, i_file, input_df_path):

        '''
        Get information about single file shower which generate a given single image
        :param input_images_path:  path to a file with images
        :param input_df_path:      path to folder with xlsx file with shower features, used to generate images
        :return: showers_dict - dictionary with shower info. as: energy, asum, particle type, etc
        '''

        showers_dict = {}

        shower_data = 'NaN'
        energy_tmp = np.NaN
        asum_tmp = np.NaN
        off_axis_tmp = np.NaN
        phi_tmp = np.NaN

        head, tail = os.path.split(i_file)
        found_data = self.return_data_name_all(i_file)

        module_pixel_map = {}

        if found_data['shower_type'] != 'muon':

            df_file = self.find_xlsx_file(
                input_df_path[0] + '/{:}_nsbx1/'.format(found_data['shower_type']) + input_df_path[1],
                found_data['run'])
                
            try:
                shower_data = self.get_shower_data(df_file, found_data['id'])
                energy_tmp = float(shower_data['energy TeV.'])
                asum_tmp = float(shower_data['asum: max amplitude'])
                off_axis_tmp = np.NaN
                phi_tmp = np.NaN
            except:
                shower_data = 'NaN'
                energy_tmp = np.NaN
                asum_tmp = np.NaN
                off_axis_tmp = np.NaN
                phi_tmp = np.NaN

        else:
            muon_path = input_df_path[0] + '/{:}_nsbx1/'.format(found_data['shower_type']) + input_df_path[1]
            df_file = self.get_muon_exel_file(found_data, muon_path)
            shower_data = self.get_shower_data(muon_path + '/' + df_file, found_data['id'])
            try:
                energy_tmp = float(found_data['energy TeV.'])
                asum_tmp = float(shower_data['asum: max amplitude']) #np.NaN
                off_axis_tmp = float(found_data['off_axis'])
                phi_tmp = float(found_data['phi'])
            except:
                shower_data = 'NaN'
                energy_tmp = np.NaN
                asum_tmp = np.NaN
                off_axis_tmp = np.NaN
                phi_tmp = np.NaN

        showers_dict = self.update_dictionary(showers_dict, 'shower_type', found_data['shower_type'])
        showers_dict = self.update_dictionary(showers_dict, 'energy TeV.', energy_tmp)
        showers_dict = self.update_dictionary(showers_dict, 'asum', asum_tmp)
        showers_dict = self.update_dictionary(showers_dict, 'off_axis', off_axis_tmp)
        showers_dict = self.update_dictionary(showers_dict, 'phi', phi_tmp)

        return showers_dict

    def get_data_statistics(self, input_images_path, input_df_path):

        '''
        Get information about shower which generate a given image
        :param input_images_path:  path to folder with images
        :param input_df_path:      path to folder with xlsx file with shower features, used to generate images
        :return: showers_dict - dictionary with shower info. as: energy, asum, particle type, etc
        '''

        input_files = self.get_all_files(input_images_path)
        print('n files : ', len(input_files))
        #input_files = input_files[:10]
        showers_dict = {}

        shower_data = 'NaN'
        energy_tmp = np.NaN
        asum_tmp = np.NaN
        off_axis_tmp = np.NaN
        phi_tmp = np.NaN

        for i_file in input_files:

            head, tail = os.path.split(input_images_path + i_file)
            found_data = self.return_data_name_all(input_images_path + i_file)

            module_pixel_map = {}

            if found_data['shower_type'] != 'muon':
                #print('found_data : ', found_data)
                df_file = self.find_xlsx_file(
                    input_df_path[0] + '/{:}_nsbx1/'.format(found_data['shower_type']) + input_df_path[1],
                    found_data['run'])
                #print(input_df_path[0] + '/{:}_nsbx1/'.format(found_data['shower_type']) + input_df_path[1],
                #    found_data['run'])

                #shower_data = self.get_shower_data(df_file, found_data['id'])
                try:
                    shower_data = self.get_shower_data(df_file, found_data['id'])
                    #print('shower_data : ', shower_data)
                    energy_tmp = float(shower_data['energy TeV.'])
                    #print('energy_tmp : ', energy_tmp)
                    asum_tmp = float(shower_data['asum: max amplitude'])
                    #print('asum_tmp : ', asum_tmp)
                    off_axis_tmp = np.NaN
                    phi_tmp = np.NaN
                except:
                    shower_data = 'NaN'
                    energy_tmp = np.NaN
                    asum_tmp = np.NaN
                    off_axis_tmp = np.NaN
                    phi_tmp = np.NaN

            else:
                try:
                    energy_tmp = float(found_data['energy TeV.'])
                    asum_tmp = np.NaN
                    off_axis_tmp = float(found_data['off_axis'])
                    phi_tmp = float(found_data['phi'])
                except:
                    shower_data = 'NaN'
                    energy_tmp = np.NaN
                    asum_tmp = np.NaN
                    off_axis_tmp = np.NaN
                    phi_tmp = np.NaN

            showers_dict = self.update_dictionary(showers_dict, 'shower_type', found_data['shower_type'])
            showers_dict = self.update_dictionary(showers_dict, 'energy TeV.', energy_tmp)
            showers_dict = self.update_dictionary(showers_dict, 'asum', asum_tmp)
            showers_dict = self.update_dictionary(showers_dict, 'off_axis', off_axis_tmp)
            showers_dict = self.update_dictionary(showers_dict, 'phi', phi_tmp)

        return showers_dict

    def update_dictionary(self, my_dict, key, data):

        '''
        Update dictionary with new data
        :param my_dict: dictionary to be updated
        :param key:     key to be updated
        :param data:    data to be save for a given key
        :return:
        '''

        try:
            tmp_list = my_dict[key]
            tmp_list.append(data)
        except KeyError:
            tmp_list = [data]

        my_dict[key] = tmp_list

        return my_dict
