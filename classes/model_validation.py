import os
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras import Input
from pathlib import Path
import numpy as np
from os import listdir
from os.path import isfile, join
import os.path
from os import path

import seaborn as sns
from sklearn.metrics import confusion_matrix
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from model import model_generator
import json

class model_validation(model_generator):

    '''
    Class for model validation
    '''
    
    def __init__(self):
        print('model validation start')

    def read_model_json_data(self, file_input):

        # Opening JSON file
        self.file_json = open(file_input)
        self.data_json = json.load(self.file_json)
        self.data_json['n_epoch'] = np.arange(0, len(self.data_json['accuracy']), 1)

    def read_json_data(self, file_input):

        # Opening JSON file
        self.file_json_data = open(file_input)
        self.data_json_data = json.load(self.file_json_data)
        

    def predict_data(self, model, test_gen):

        '''
        Predict data class from the trained model
        :param model:    trained model
        :param test_gen: test data from ImageDataGenerator
        :return:
        '''

        true_classes = test_gen.classes
        class_indices = test_gen.class_indices
        class_indices = dict((v,k) for k,v in class_indices.items())

        preds = model.predict(test_gen)
        pred_classes = np.argmax(preds, axis=1)

        class_names = test_gen.class_indices.keys()

        return true_classes, pred_classes, class_names
    
    def plot_heatmap(self, y_true, y_pred, class_names, ax, title):

        '''
        Plot heatmap
        :param y_true: - true class
        :param y_pred: - predicted class
        :param class_names: clases name
        :param ax:
        :param title:
        :return:
        '''
        
        cm = confusion_matrix(y_true, y_pred)
        sns.heatmap(cm, annot=True, square=True, xticklabels=class_names,  yticklabels=class_names, fmt='d', cmap=plt.cm.Blues, cbar=False, ax=ax)
        ax.set_title(title, fontsize=16)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha="right")
        ax.set_ylabel('True Label', fontsize=12)
        ax.set_xlabel('Predicted Label', fontsize=12)
