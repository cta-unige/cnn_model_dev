import os
import json
import numpy as np
import matplotlib.pyplot as plt
import getopt
import sys
from classes.prepare_data import data_split

sowers = data_split()


param_line = '-d <data_type> -c <class_type>'

data_type =''
class_type =''

try:
    opts, args = getopt.getopt(sys.argv[1:], "hd:c:", ["data_type", "class_type"])
except getopt.GetoptError:
    print('error: ',param_line)
    sys.exit(2)

for opt, arg in opts:
    if opt == '-h':
        print(param_line)
        sys.exit()

    elif opt in ("-d", "--data_type"):
        data_type = arg

    elif opt in ("-c", "--class_type"):
        class_type = arg

print('input data type  : ', data_type)
print('input class type : ', class_type)


#data_path = '/home/nagaia/scratch/data/clipped_data/4_classes/'+ data_type + '/' + class_type + '/'
data_path = '/home/nagaia/scratch/data/clipped_data/4_classes/anomaly_011/' + class_type + '/'
#data_path = '/home/nagaia/scratch/data/clipped_data/4_classes/train/' + class_type + "/"
#input_files = sowers.get_all_files(data_path)
#print('n files : ',len(input_files))

used_showers = sowers.get_data_statistics(data_path, ['/home/nagaia/scratch/data/', '/anomaly_data/'])

with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/4_clases_'+ data_type + '_' + class_type +'_anomaly_011_nsb_200k.json', 'w') as f:
    json.dump(used_showers, f)
