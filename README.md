# cnn_model_dev

This project was developed to train and test different CNN models (i.e. ResNet and VGG16) on shower images (gammas on/diffused, electrons, protons Muons) and NSB. Also, basic scripts are presented to prepare data (i.e. train, validation and test split) as vell as model performance analysis. Project uses data created by [shower_generator](https://gitlab.cern.ch/cta-unige/shower_generator) project.

Data Flow Diagram can be found here: [data flow diagram](https://lucid.app/lucidchart/23ab3466-0dfa-4dcd-95ea-2716589799ce/edit?viewport_loc=-55%2C244%2C2219%2C1116%2C0_0&invitationId=inv_7e49648f-a868-4652-8443-2facc041a367#)

## Structure:

Project has a few folders:
1. **Classes** -> Folder with developed clases:
   1. **model.py** -> classe to create CNN model;
   2. **model_validation.py** -> classe for model validation;
   3. **prepare_data.py** -> class to work with images, like copy, move and split data. Also, to colect information about showers which where used to generate images, like: energy, direction, type, analog sum and etc.
2. **jobs** -> folder with job example for *yggdrasil* Unige server.
3. **notebooks** -> folder with jupiter notebook collection for data analysis
4. scripts are in main folder.

## Dependency:

Images developed by [shower_generator](https://gitlab.cern.ch/cta-unige/shower_generator) project needed.

## Usage

1. Convert [sim_telarray](https://www.mpi-hd.mpg.de/hfm/~bernlohr/sim_telarray/) *.gz* files into images, by using [shower_generator](https://gitlab.cern.ch/cta-unige/shower_generator);
2. Split data into desired clasess (i.e NSB, Gammas, electrons, protons, etc.) using *split_data_script.py* script;
3. Split data into *train*, *validate* and *test* using *split_data_script.py* script;
4. Train model, using the *model_train_script.py* script. The job example can be found at: *jobs/job_train_resnet_50.job*;
5. Validate model, using the *validate_model_script.py* script. The job example can be found at: *jobs/job_test_model_resnet.job*;
6. Study, whcih showers were classified correctly and which not. To do so, run *validate_model_script_and_save_stat_data.py* script. It will create the *json* files with shower info.
7. To get information about shower (i.e. energy, asum, etc.) one should use the script: *particle_types_script.py*, whcih creates the *.json* file with shower info. To use it, one can run *jobs/job_data_statistics.job*


## Authors and acknowledgment
Developed by A. Nagai at University of Geneve, DPNC

## License
For open source projects, say how it is licensed.

