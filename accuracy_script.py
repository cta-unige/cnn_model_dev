import os, sys
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os.path
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import numpy as np
import json

from classes.model_validation import model_validation
from classes.prepare_data import data_split

my_model = model_validation()
input_shape = (234, 234, 3)
optim_1 = Adam(learning_rate=0.001)
n_classes=4

test_model = my_model.resnet_2stages(input_shape, n_classes, optim_1)
test_model.load_weights('/home/nagaia/ctasoft/scripts/cnn_model_dev/resNet_2stg_v5_4cl_270deg_clipped.weights.best.hdf5')

test_data_dir = '/home/nagaia/scratch/data/clipped_data/4_classes/test/'
#test_data_dir = '/home/nagaia/scratch/data/clipped_data/2_classes/test/'
test_generator = ImageDataGenerator(preprocessing_function=preprocess_input)

testgen = test_generator.flow_from_directory(test_data_dir,
                                             target_size=(234, 234),
                                             class_mode=None,
                                             batch_size=1,
                                             shuffle=False,
                                             seed=42)

true_classes = testgen.classes
class_indices = testgen.class_indices
class_indices = dict((v,k) for k,v in class_indices.items())

model_preds = test_model.predict(testgen)
model_pred_classes = np.argmax(model_preds, axis=1)

print('true classes : ', true_classes)
print('predict classes : ', model_pred_classes)

model_acc = accuracy_score(true_classes, model_pred_classes)
print("Model Accuracy: {:.2f}%".format(model_acc * 100))

shower_reco_wrong = []
nsb_reco_wrong = []

showers_correct_reco = []
nsb_correct_reco = []

for item, (t_class, p_class) in enumerate(zip(true_classes, model_pred_classes)):
    if t_class != p_class:
        #print(item, ' ', t_class, ' ', p_class, ' ', testgen.filenames[item])
        if t_class == 0:
            nsb_reco_wrong.append(test_data_dir + testgen.filenames[item])
        else:
            shower_reco_wrong.append(test_data_dir + testgen.filenames[item])
    else:
        if t_class == 0:
            nsb_correct_reco.append(test_data_dir + testgen.filenames[item])
        else:
            showers_correct_reco.append(test_data_dir + testgen.filenames[item])

sowers = data_split()

print("showers_correct_reco : ", showers_correct_reco)

data_shower_correct = sowers.get_data_statistics_single_file(showers_correct_reco, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/data_test_011_showrs_correct.json', 'w') as f:
    json.dump(data_shower_correct, f)

#data_shower_wrong = sowers.get_data_statistics_single_file(shower_reco_wrong, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
#with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/data_test_011_showrs_wrong.json', 'w') as f:
#    json.dump(data_shower_wrong, f)

data_nsb_correct = sowers.get_data_statistics_single_file(nsb_correct_reco, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/data_test_011_nsb_correct.json', 'w') as f:
    json.dump(data_nsb_correct, f)

#data_nsb_wrong = sowers.get_data_statistics_single_file(nsb_reco_wrong, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
#with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/data_test_011_nsb_wrong.json', 'w') as f:
#    json.dump(data_nsb_wrong, f)
