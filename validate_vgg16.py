import os, sys
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os.path
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import numpy as np
from classes.model_validation import model_validation

my_model = model_validation()


input_shape = (234, 234, 3)
optim_1 = Adam(learning_rate=0.001)
n_classes=4
number_of_filters = 12
filter_size = 3

# First we'll train the model without Fine-tuning
vgg_model = my_model.resnet50(input_shape, n_classes, optim_1)
vgg_model.load_weights('/home/nagaia/ctasoft/scripts/cnn_model_dev/resNet_50_v2_4cl_270deg_clipped.weights.best.hdf5')

test_data_dir = '/home/nagaia/scratch/data/clipped_data/4_classes/test/'
test_generator = ImageDataGenerator(preprocessing_function=preprocess_input)

testgen = test_generator.flow_from_directory(test_data_dir,
                                             target_size=(234, 234),
                                             class_mode=None,
                                             batch_size=1,
                                             shuffle=False,
                                             seed=42)


#class_indices_020 = testgen.class_indices
#class_indices_020 = dict((v,k) for k,v in class_indices_020.items())

#true_classes_an_020, pred_classes_an_020, classes_names_an_020 = my_model.predict_data(vgg_model, testgen)

#vgg_acc_ft = accuracy_score(true_classes_an_020, pred_classes_an_020)
#print("VGG16 Model Accuracy with Fine-Tuning: {:.2f}%".format(vgg_acc_ft * 100))

#vgg_model.load_weights('tl_model_v1.weights.best.hdf5') # initialize the best trained weights



true_classes = testgen.classes
class_indices = testgen.class_indices
class_indices = dict((v,k) for k,v in class_indices.items())

vgg_preds = vgg_model.predict(testgen)
vgg_pred_classes = np.argmax(vgg_preds, axis=1)

vgg_acc = accuracy_score(true_classes, vgg_pred_classes)
print("VGG16 Model Accuracy without Fine-Tuning: {:.2f}%".format(vgg_acc * 100))

class_names = testgen.class_indices.keys()

fig, ax3 = plt.subplots(1, 1, figsize=(10, 10))
my_model.plot_heatmap(true_classes, vgg_pred_classes, class_names, ax3, title="Transfer Learning (VGG16)")

fig.tight_layout()
#fig.subplots_adjust(top=3.25)
plt.savefig(os.path.join('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/resNet_50_v2_4cl_270deg_clipped_test_gen_020.png'), dpi=800)
#plt.show()
