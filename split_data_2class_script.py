from classes.prepare_data import data_split

path_in = '/home/nagaia/scratch/data/'
image_type = '/clipped/'

path_nsb_out = '/home/nagaia/scratch/data/clipped_data/2_classes/anomaly_0.11/nsb/'
path_out_shower = '/home/nagaia/scratch/data/clipped_data/2_classes/anomaly_0.11/shower/'
path_out_muons = path_out_shower

data_sp = data_split()

# copy showers :
data_sp.copy_some_showers(path_in, path_out_shower, image_type, ['gamma_diffuse_nsbx1', 'gamma_on_nsbx1', 'proton_nsbx1', 'electron_nsbx1'], 0.11, 5000)

# copy muons :
data_sp.copy_some_files('/home/nagaia/scratch/data/muons/run1/clipped/', path_out_muons, 2500)
data_sp.copy_some_files('/home/nagaia/scratch/data/muons/run2/clipped/', path_out_muons, 2500)

# copy nsb :
data_sp.copy_all_types(path_in, path_nsb_out, image_type, 0.001, 6250)


# create test, train and validation
#data_sp.split_data(path_out_shower, '/home/nagaia/scratch/data/clipped_data/4_classes/small_data_set/', 'shower/')
#data_sp.split_data(path_out_proton, '/home/nagaia/scratch/data/clipped_data/4_classes/small_data_set/', 'proton/')
#data_sp.split_data(path_out_muons, '/home/nagaia/scratch/data/clipped_data/4_classes/small_data_set/', 'muon/')
#data_sp.split_data(path_nsb_out, '/home/nagaia/scratch/data/clipped_data/4_classes/small_data_set/', 'nsb/')
