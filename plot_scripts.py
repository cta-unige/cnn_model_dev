import os
import json
import numpy as np
import matplotlib.pyplot as plt
from classes.model_validation import model_validation

train_shower_file = 'train_shower.json'
test_shower_file = 'test_shower.json'
validation_shower_file = 'validation.json'

data_shower_train = model_validation()
data_shower_train.read_json_data(train_shower_file)

data_shower_validate = model_validation()
data_shower_validate.read_json_data(validation_shower_file)

data_shower_test = model_validation()
data_shower_test.read_json_data(test_shower_file)

plt.figure(1)
plt.hist(data_shower_train.data_json_data['shower_type'], alpha = 0.5, label='train')
plt.hist(data_shower_validate.data_json_data['shower_type'], alpha = 0.5, label='validation')
plt.hist(data_shower_test.data_json_data['shower_type'], alpha = 0.5, label='test')
plt.grid()
plt.legend()
plt.show()

plt.figure(2)
plt.hist(data_shower_train.data_json_data['energy TeV.'], range=[0.01, 100], bins=100, alpha = 0.5, label='train')
plt.hist(data_shower_validate.data_json_data['energy TeV.'], range=[0.01, 100], bins=100, alpha = 0.5, label='validation')
plt.hist(data_shower_test.data_json_data['energy TeV.'], range=[0.01, 100], bins=100, alpha = 0.5, label='test')
plt.yscale('log')
plt.xscale('log')
plt.grid()
plt.legend()
plt.show()

plt.figure(3)
plt.hist(data_shower_train.data_json_data['asum'], bins=100, range=[200, 10000], alpha = 0.5, label='train')
plt.hist(data_shower_validate.data_json_data['asum'], bins=100, range=[200, 10000], alpha = 0.5, label='validation')
plt.hist(data_shower_test.data_json_data['asum'], bins=100, range=[200, 10000], alpha = 0.5, label='test')
plt.yscale('log')
#plt.xscale('log')
#plt.yscale('log')
plt.legend()
plt.grid()
plt.show()


train_file = 'vgg16_1l_pooling_7x7/vgg16_gpu_1_layer_12x3x3.json'
train_hist = model_validation()
train_hist.read_model_json_data(train_file)

plt.figure(4)
plt.plot(train_hist.data_json['n_epoch'], train_hist.data_json['accuracy'], label='train')
plt.plot(train_hist.data_json['n_epoch'], train_hist.data_json['val_accuracy'], label='validation')
plt.grid()
plt.legend()
plt.show()

plt.figure(5)
plt.plot(train_hist.data_json['n_epoch'], train_hist.data_json['loss'], label='train')
plt.plot(train_hist.data_json['n_epoch'], train_hist.data_json['val_loss'], label='validation')
plt.grid()
plt.legend()
plt.show()
