import os, sys
import pandas as pd
import os.path
from os import listdir
import glob
import matplotlib.pyplot as plt
import numpy as np
import json
from os.path import isfile, join
import matplotlib.pyplot as plt
import getopt
import sys
from numpy import load
from classes.prepare_data import data_split

my_data_class = data_split()


true_clases_2stg_4k = load('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/resnet_2stg_true_classes_data_200k_nsb.npy')
predict_clases_2stg_4k = load('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/resnet_2stg_model_pred_classes_data_200k_nsb.npy')
files_2stg_4k = load('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/resnet_2stg_files_200k_nsb.npy')

model_pred_classes_2stg_4k = np.argmax(predict_clases_2stg_4k, axis=1)

'''
muons_as_nsb = my_data_class.get_wrong_reco(model_pred_classes_2stg_4k, true_clases_2stg_4k, files_2stg_4k, 0, 1)
proton_as_nsb = my_data_class.get_wrong_reco(model_pred_classes_2stg_4k, true_clases_2stg_4k, files_2stg_4k, 2, 1)
shower_as_nsb = my_data_class.get_wrong_reco(model_pred_classes_2stg_4k, true_clases_2stg_4k, files_2stg_4k, 3, 1)

muon_as_nsb_dict = {}
proton_as_nsb_dict = {}
shower_as_nsb_dict = {}

muon_as_nsb_dict = my_data_class.get_wrong_dict(muons_as_nsb, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/4_clases_muons_as_nsb_dict.json', 'w') as f:
    json.dump(muon_as_nsb_dict, f)

proton_as_nsb_dict = my_data_class.get_wrong_dict(proton_as_nsb, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/4_clases_protons_as_nsb_dict.json', 'w') as f:
    json.dump(proton_as_nsb_dict, f)

shower_as_nsb_dict = my_data_class.get_wrong_dict(shower_as_nsb, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/4_clases_showers_as_nsb_dict.json', 'w') as f:
    json.dump(shower_as_nsb_dict, f)

'''

nsb_as_muons = my_data_class.get_wrong_reco(model_pred_classes_2stg_4k, true_clases_2stg_4k, files_2stg_4k, 1, 0)
nsb_as_proton = my_data_class.get_wrong_reco(model_pred_classes_2stg_4k, true_clases_2stg_4k, files_2stg_4k, 1, 2)
nsb_as_shower = my_data_class.get_wrong_reco(model_pred_classes_2stg_4k, true_clases_2stg_4k, files_2stg_4k, 1, 3)

nsb_as_muon_dict = {}
nsb_as_proton_dict = {}
nsb_as_shower_dict = {}

nsb_as_muon_dict = my_data_class.get_wrong_dict(nsb_as_muons, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/4_clases_nsb_as_muons_dict_200k_nsb.json', 'w') as f:
    json.dump(nsb_as_muon_dict, f)

nsb_as_proton_dict = my_data_class.get_wrong_dict(nsb_as_proton, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/4_clases_nsb_as_protons_dict_200k_nsb.json', 'w') as f:
    json.dump(nsb_as_proton_dict, f)

nsb_as_shower_dict = my_data_class.get_wrong_dict(nsb_as_shower, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
with open('/home/nagaia/ctasoft/scripts/cnn_model_dev/ResNet/4class/data_types/4_clases_nsb_as_showers_dict_200k_nsb.json', 'w') as f:
    json.dump(nsb_as_shower_dict, f)
