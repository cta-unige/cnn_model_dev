import os, sys
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os.path
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import numpy as np
import json
from classes.model_validation import model_validation
from classes.prepare_data import data_split

sowers = data_split()

showers_correct_reco = ['/home/nagaia/scratch/data/clipped_data/2_classes/anomaly_0.11/shower/clipped_electron_run_836_id_53801.jpg', '/home/nagaia/scratch/data/clipped_data/2_classes/anomaly_0.11/shower/clipped_electron_run_836_id_53801.jpg', '/home/nagaia/scratch/data/clipped_data/2_classes/anomaly_0.11/shower/clipped_proton_run_1367_id_4096008.jpg', '/home/nagaia/scratch/data/clipped_data/2_classes/anomaly_0.11/shower/clipped_electron_run_1120_id_215706.jpg']

data_shower_correct = sowers.get_data_statistics_single_file(showers_correct_reco, ['/home/nagaia/scratch/data/', '/anomaly_data/'])

print(data_shower_correct)

