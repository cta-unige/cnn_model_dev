import os, sys
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.applications.vgg16 import VGG16, preprocess_input
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os.path
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score
import numpy as np
import getopt

from classes.model_validation import model_validation

param_line = '-i <input_path> -m <model> -o <output_path> -w <weight>'

try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:w:m:", ["input_path", "output_path", "weight", "model"])
except getopt.GetoptError:
    print('error: ',param_line)
    sys.exit(2)

for opt, arg in opts:
    if opt == '-h':
        print(param_line)
        sys.exit()

    elif opt in ("-i", "--input_path"):
        input_path = arg

    elif opt in ("-o", "--output_path"):
        output_path = arg

    elif opt in ("-w", "--weight"):
        weights = arg
    
    elif opt in ("-m", "--model"):
        model_to_use = arg

my_model = model_validation()


input_shape = (234, 234, 3)
optim_1 = Adam(learning_rate=0.001)
n_classes=4
n_layer_tune=0

defualt_weights = '/home/nagaia/ctasoft/scripts/cnn_model_dev/vgg16_default_model_weights/vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5'


if model_to_use == "vgg16":
    print('use vgg16 model')
    try:
        model_1l = my_model.create_vgg16_model(weights, input_shape, n_classes, optim_1, n_layer_tune)
        print('vgg16 :: load veights')
    except:
        model_1l = my_model.create_vgg16_model(defualt_weights, input_shape, n_classes, optim_1, n_layer_tune)
        print('vgg16 :: use def weights')

elif model_to_use == "resnet_2stg":
    print('use ResNet model with 2 stages')
    model_1l = my_model.resnet_2stages(input_shape, n_classes, optim_1)

elif model_to_use == "resnet_1stg":
    print('use ResNet model with 1 stages')
    model_1l = my_model.resnet_1stages(input_shape, n_classes, optim_1)

elif model_to_use == "resnet_50":
    print('use ResNet 50 model')
    model_1l = my_model.resnet50(input_shape, n_classes, optim_1)

else:
    print("Error ::  Unknown Model :: ", model_to_use)
    sys.exit()

try:
    #if (model_to_use != "vgg16"):
    model_1l.load_weights(weights)
    print('loaded weights : ', weights)
except:
    print('no weights were specified')

print(model_1l.summary())



test_generator = ImageDataGenerator(preprocessing_function=preprocess_input)

testgen = test_generator.flow_from_directory(input_path,
                                             target_size=(234, 234),
                                             class_mode=None,
                                             batch_size=1,
                                             shuffle=False,
                                             seed=42)


true_classes = testgen.classes
class_indices = testgen.class_indices
class_indices = dict((v,k) for k,v in class_indices.items())

model_preds = model_1l.predict(testgen)
model_pred_classes = np.argmax(model_preds, axis=1)

model_acc = accuracy_score(true_classes, model_pred_classes)
print("Model Accuracy : {:.2f}%".format(model_acc * 100))

class_names = testgen.class_indices.keys()

fig, ax3 = plt.subplots(1, 1, figsize=(10, 10))
my_model.plot_heatmap(true_classes, model_pred_classes, class_names, ax3, title="")

fig.tight_layout()
#fig.subplots_adjust(top=3.25)
plt.savefig(os.path.join(output_path + '/results_011_' + str(model_to_use) +'_.png'), dpi=200)
#plt.show()
