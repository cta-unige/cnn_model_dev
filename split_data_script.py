from classes.prepare_data import data_split

path_in = '/home/nagaia/scratch/data/mono-lst-sipm-pmma-3ns-v1_triggerless/'
path_in_nsb = '/home/nagaia/scratch/data/'
image_type = '/clipped/'

path_out_gamma_on = '/home/nagaia/scratch/data/clipped_data/6_classes/anomaly_011/gamma_on/'
path_out_gamma_diffuse = '/home/nagaia/scratch/data/clipped_data/6_classes/anomaly_011/gamma_diffuse/'
path_out_proton = '/home/nagaia/scratch/data/clipped_data/6_classes/anomaly_011/proton/'
path_out_electron = '/home/nagaia/scratch/data/clipped_data/6_classes/anomaly_011/electron/'
path_out_muon = '/home/nagaia/scratch/data/clipped_data/6_classes/anomaly_011/muon/'
path_out_nsb = '/home/nagaia/scratch/data/clipped_data/6_classes/anomaly_011/nsb/'

train_test_validation_out = '/home/nagaia/scratch/data/clipped_data/6_classes/anomaly_011/data_for_model_run1/'

data_sp = data_split()

# copy gamma_on :
#data_sp.copy_some_showers(path_in, path_out_shower, image_type, ['gamma_diffuse_nsbx1', 'gamma_on_nsbx1', 'electron_nsbx1'], 0.11, 30000)
#data_sp.copy_some_showers(path_in, path_out_gamma_on, image_type, ['gamma_on_nsb_1x'], 0.11, 500000)

# copy gamma_diffuse :
#data_sp.copy_some_showers(path_in, path_out_gamma_diffuse, image_type, ['gamma_diffuse_nsb_1x'], 0.11, 500000)

# copy proton :
#data_sp.copy_some_showers(path_in, path_out_proton, image_type, ['proton_nsb_1x'], 0.11, 500000)

# copy electron :
#data_sp.copy_some_showers(path_in, path_out_electron, image_type, ['electron_nsb_1x'], 0.11, 500000)


# copy muons :
#data_sp.copy_some_files('/home/nagaia/scratch/data/muon_nsbx1/run1/clipped/', path_out_muon, 50000)
#data_sp.copy_some_files('/home/nagaia/scratch/data/muon_nsbx1/run2/clipped/', path_out_muon, 50000)

# copy nsb :
data_sp.copy_all_types(path_in_nsb, path_out_nsb, image_type, 0.001, 5000000)


# create test, train and validation
#data_sp.split_data(path_out_gamma_on, train_test_validation_out, 'gamma_on/')
#data_sp.split_data(path_out_gamma_diffuse, train_test_validation_out, 'gamma_diffuse/')
#data_sp.split_data(path_out_proton, train_test_validation_out, 'proton/')
#data_sp.split_data(path_out_electron, train_test_validation_out, 'electron/')
#data_sp.split_data(path_out_muon, train_test_validation_out, 'muon/')
data_sp.split_data(path_out_nsb, train_test_validation_out, 'nsb/')

