import os, sys
import json

import os.path
import matplotlib.pyplot as plt
import numpy as np
import getopt
from classes.prepare_data import data_split

def read_json_data(file_input):

        # Opening JSON file
    file_json_data = open(file_input)
    data_json_data = json.load(file_json_data)
    
    return data_json_data

param_line = '-i <input_path> -o <output_path>'
drop_out = 0.


try:
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:", ["input_path", "output_path"])
except getopt.GetoptError:
    print('error: ',param_line)
    sys.exit(2)

for opt, arg in opts:
    if opt == '-h':
        print(param_line)
        sys.exit()

    elif opt in ("-i", "--input_path"):
        input_path = arg

    elif opt in ("-o", "--output_path"):
        output_path = arg

files = read_json_data(input_path)
#print(read_json_data(input_path))

head, tail = os.path.split(input_path)
#print('file : ', tail)

sowers = data_split()
used_showers = sowers.get_data_statistics_single_file(files, ['/home/nagaia/scratch/data/', '/anomaly_data/'])
#print(used_showers)

with open(output_path + tail[:-5] + '_results.json', 'w') as f:
    json.dump(used_showers, f)



